import { createStore, applyMiddleware, compose } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware } from 'connected-react-router';
import thunk from 'redux-thunk';
import createRootReducer from './reducers';

export const history = createBrowserHistory();
export default function configureStore() {
    const initialState = {};
    const enhancers = [];
    const middleware = [thunk, routerMiddleware(history)];

    if (process.env.NODE_ENV === 'development') {
        const devToolsExtension = window.devToolsExtension;

        if (typeof devToolsExtension === 'function') {
            enhancers.push(devToolsExtension());
        }
    }

    const composedEnhancers = compose(applyMiddleware(...middleware), ...enhancers);

    const store = createStore(createRootReducer(history), initialState, composedEnhancers);
    return store;
}
