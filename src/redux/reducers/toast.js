import { toastConstants } from '../actionTypes';

const { ADD_TOAST, REMOVE_TOAST } = toastConstants;
// const initialToast = [
//     {
//         variant: 'success',
//         text: 'This is sample toast message!',
//         id: 1
//     }
// ]

export default (state = [], { type, payload } = action) => {
    switch (type) {
        case ADD_TOAST:
            return [
                ...state,
                payload
            ]
        case REMOVE_TOAST:
            return state.filter(toast => toast.id !== payload);
        default:
            return state;
    }
}