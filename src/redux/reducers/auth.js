import { authConstants } from '../actionTypes';
const {
    SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_ERROR,
    SIGNIN_REQUEST, SIGNIN_SUCCESS, SIGNIN_ERROR,
    FETCH_PROFILE_REQUEST, FETCH_PROFILE_SUCCESS, FETCH_PROFILE_ERROR,
    LOGOUT, AUTO_LOGIN_ERROR
} = authConstants;

const initialState = {
    user: null,
    isLoading: false,
    autoLoggingIn: true,
    error: null,
    signedUp: false,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PROFILE_REQUEST:
        case SIGNIN_REQUEST:
        case SIGNUP_REQUEST:
            return {
                ...state,
                error: null,
                isLoading: true,
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                signedUp: true,
                error: null,
                isLoading: false,
                expiredToken: false,
            };
        case FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                autoLoggingIn: false,
                isLoading: false,
                user: action.data,
            };
        case AUTO_LOGIN_ERROR:
        case FETCH_PROFILE_ERROR:
        case SIGNIN_ERROR:
        case SIGNUP_ERROR:
            return {
                ...state,
                user: null,
                autoLoggingIn: false,
                isLoading: false,
                error: action.error,
            };
        case SIGNIN_SUCCESS:
            return state;
        case LOGOUT:
            return {
                ...state,
                user: null
            }
        default:
            return state;
    }
}