import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import authReducer from './auth';
import layoutReducer from './layout';
import toastReducer from './toast';

export default history => combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    layout: layoutReducer,
    toasts: toastReducer
})

