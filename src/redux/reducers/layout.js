import { layoutConstants } from '../actionTypes';
const { SHOW_PRELOADER, HIDE_PRELOADER } = layoutConstants;


const initialState = {
    preloader: false,
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SHOW_PRELOADER:
            return {
                ...state,
                preloader: true
            }
        case HIDE_PRELOADER:
            return {
                ...state,
                preloader: false
            }
        default:
            return state;
    }
}