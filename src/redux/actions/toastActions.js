import { toastConstants } from '../actionTypes';
import { toastHelper } from '../../utils';

const {
    ADD_TOAST,
    REMOVE_TOAST
} = toastConstants;

export const dispatchToast = (options = {}) => {
    let { error, variant } = options;

    if (error) {
        let errorMessage = error.response && error.response.data && error.response.data.message || '';
        return {
            type: ADD_TOAST,
            payload: toastHelper.createToast({ text: errorMessage || error.message, variant })
        }
    }
}


export const removeToast = (toastId) => {
    return {
        type: REMOVE_TOAST,
        payload: toastId
    }
}

