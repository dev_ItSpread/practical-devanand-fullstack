import { authConstants, layoutConstants } from '../actionTypes';
import { authManager as AuthManager } from '../../utils';
import axios from 'axios';
import { push } from 'connected-react-router';
import { dispatchToast } from './toastActions';

const {
    SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_ERROR,
    SIGNIN_REQUEST, SIGNIN_SUCCESS, SIGNIN_ERROR,
    FETCH_PROFILE_REQUEST, FETCH_PROFILE_SUCCESS, FETCH_PROFILE_ERROR,
    LOGOUT, AUTO_LOGIN_ERROR
} = authConstants;

const {
    SHOW_PRELOADER, HIDE_PRELOADER
} = layoutConstants;

export const signup = user => async dispatch => {
    try {
        dispatch({ type: SIGNUP_REQUEST });
        dispatch({ type: SHOW_PRELOADER });

        const url = '/api/users/register';
        const { data } = await axios({
            method: 'POST',
            url,
            data: user
        })

        dispatch({ type: SIGNUP_SUCCESS, data });
        dispatch({ type: HIDE_PRELOADER });
        //Save authentication data later
        AuthManager.saveAuthentication(data);
        await dispatch(fetchProfile());
    } catch (error) {
        dispatch(dispatchToast({ error, variant: 'error' }))
        dispatch({ type: HIDE_PRELOADER });
        dispatch({ type: SIGNUP_ERROR, error });
    }
}

export const signin = user => async dispatch => {
    try {
        dispatch({ type: SIGNIN_REQUEST });
        dispatch({ type: SHOW_PRELOADER });

        const url = '/api/authenticate';
        const { data } = await axios({
            method: 'POST',
            url,
            data: user
        })

        dispatch({ type: SIGNIN_SUCCESS, data })
        dispatch({ type: HIDE_PRELOADER });
        //Save authentication data later
        AuthManager.saveAuthentication(data);
        await dispatch(fetchProfile());
    } catch (error) {
        dispatch({ type: HIDE_PRELOADER });
        dispatch(dispatchToast({ error, variant: 'error' }))
        dispatch({ type: SIGNIN_ERROR, error });
    }
}

export const fetchProfile = () => async dispatch => {
    try {
        dispatch({ type: FETCH_PROFILE_REQUEST });

        const url = '/api/users/profile';
        const { data } = await axios({
            method: "GET",
            url,
            headers: {
                Authorization: AuthManager.getBearerToken()
            }
        })

        dispatch({ type: FETCH_PROFILE_SUCCESS, data: data });
    } catch (error) {
        dispatch(dispatchToast({ error, variant: 'error' }))
        dispatch({ type: FETCH_PROFILE_ERROR, error });
    }
}

export const autoLogin = () => async dispatch => {
    if (AuthManager.isAuthenticated()) {
        await dispatch(fetchProfile());
    } else {
        dispatch({ type: AUTO_LOGIN_ERROR })
    }
}

export const logout = () => dispatch => {
    AuthManager.clearAuthentication();
    dispatch({ type: LOGOUT })
}

export const handleExpiredToken = () => dispatch => {
    dispatch(
        push({
            pathname: '/login',
            state: {
                message: "Sorry, session signed out for your protection."
            }
        })
    )
    dispatch(logout());
}