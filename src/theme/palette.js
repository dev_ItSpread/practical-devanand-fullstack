import { colors } from '@material-ui/core';

const white = '#FFFFFF';
const black = '#000000';

export default {
    black,
    white,
    primary: {
        contrastText: white,
        dark: '#201e20',
        main: '#201e20e0',
        light: '#201e20d1'
    },
    secondary: {
        contrastText: white,
        dark: '#c1bdb4',
        main: '#201e20e0',
        light: '#201e20c7'
    },
    success: {
        contrastText: white,
        dark: colors.green[900],
        main: colors.green[600],
        light: colors.green[400]
    },
    info: {
        contrastText: white,
        dark: colors.blue[900],
        main: colors.blue[600],
        light: colors.blue[400]
    },
    warning: {
        contrastText: white,
        dark: colors.orange[900],
        main: colors.orange[600],
        light: colors.orange[400]
    },
    error: {
        contrastText: white,
        dark: colors.red[900],
        main: colors.red[600],
        light: colors.red[400]
    },
    text: {
        primary: colors.blueGrey[900],
        secondary: colors.blueGrey[600],
        link: colors.blue[600]
    },
    background: {
        default: '#F4F6F8',
        paper: white,
        toolbar: 'rgb(247, 232, 228)'
    },
    icon: '#a02541',
    icon_secondary: colors.blueGrey[600],
    divider: colors.grey[200]
};
