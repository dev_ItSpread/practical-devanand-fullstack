import React, { Component } from 'react';
import {
    withStyles,
    Hidden
} from '@material-ui/core';
import Header from './header';
import Footer from './footer';
import './layout.less';

const styles = theme => ({
    root: {
        paddingTop: 55,
        height: '100%',
    },
    content: {
        height: '100%',
        background: '#f4f6f8'
    },
    childContainer: {
        margin: 'auto'
    }
})

class Layout extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        const { children, classes } = this.props;
        return (
            <div className={classes.root}>
                <Header history={this.props.history} />
                <main className={classes.content}>
                    <div className={classes.childContainer}>
                        {children}
                    </div>
                    <Hidden xsDown>
                        <Footer className="footer-container" />
                    </Hidden>
                </main>
            </div>
        );
    }
}

export default withStyles(styles)(Layout);
