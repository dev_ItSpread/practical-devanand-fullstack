import React, { Component } from 'react';
import clsx from 'clsx';
import {
    withStyles,
    Typography
} from '@material-ui/core';

const styles = theme => ({
    root: {
        padding: theme.spacing(2),
        position: 'fixed',
        bottom: 0
    },
})

class Footer extends Component {
    render() {
        const { classes, className } = this.props;

        return (
            <div className={clsx(classes.root, className)}>
                <Typography variant="caption">Copyright@2021, Express Cart. All Rights Reserved</Typography>
            </div>
        );
    }
}

export default withStyles(styles)(Footer);
