import React, { Component } from 'react';
import { AppBar, Toolbar, Hidden, IconButton, Avatar, Menu, MenuItem, withStyles, Badge } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import AccountCircle from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import LockIcon from '@material-ui/icons/Lock';
import NotificationsIcon from '@material-ui/icons/NotificationsOutlined';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { logout } from '../../redux/actions/authActions';
import Logo from '../../assets/images/logo/express_cart_logo_fourth.png';
import AddIcon from '@material-ui/icons/Add';

const styles = theme => ({
    root: {
        boxShadow: 'none',
        color: '#201e20',
        background: 'rgba(255, 255, 255, 0.87)',
        borderBottom: '1px solid rgb(238, 238, 238)'
    },
    iconColor: {
        color: "#201e20"
    },
    flexGrow: {
        flexGrow: 1
    },
    avatar: {
        width: '1em',
        height: '1em'
    },
    headerLogo: {
        height: 46
    },
})

class Header extends Component {
    constructor(props) {
        super(props)
        this.state = {
            anchorEl: null
        }
    }

    handleProfileMenuOpen = (event) => {
        this.setState({ anchorEl: event.currentTarget })
    };

    handleMenuClose = () => {
        this.setState({ anchorEl: null })
    };

    logout = () => {
        this.props.logout()
        this.handleMenuClose()
    }

    handleProfileNav = () => {
        this.props.history.push('/profile')
        this.handleMenuClose()
    }

    render() {
        const { classes, currentUser, isLoading } = this.props;
        const isMenuOpen = Boolean(this.state.anchorEl);
        const menuId = 'primary-search-account-menu';

        return (
            <React.Fragment>
                <AppBar className={classes.root}>
                    <Toolbar>
                        <RouterLink to="/">
                            <img className={classes.headerLogo} alt="Logo" src={Logo} />
                        </RouterLink>
                        <div className={classes.flexGrow} />
                        <Hidden mdDown>
                        <RouterLink to="/addStudent">
                            <IconButton color="inherit">
                                <AddIcon className={classes.iconColor} />
                            </IconButton>
                        </RouterLink>

                            <IconButton
                                edge="end"
                                aria-label="account of current user"
                                aria-controls={menuId}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                            >
                                {!isLoading && currentUser && currentUser.avatar ? (
                                    <Avatar
                                        alt="Person"
                                        className={classes.avatar}
                                        src={currentUser.avatar}
                                    />
                                ) : (
                                        <AccountCircle className={classes.iconColor} />
                                    )
                                }
                            </IconButton>
                        </Hidden>
                    </Toolbar>
                </AppBar>
                <Menu
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                    id={menuId}
                    keepMounted
                    transformOrigin={{ vertical: 'top', horizontal: 'right' }}
                    open={isMenuOpen}
                    onClose={this.handleMenuClose}
                    className="Topbar__Menu"
                >
                    <MenuItem className="topbar_menuitem" onClick={this.logout} ><ExitToAppIcon className={classes.iconColor} /> <span>Logout</span></MenuItem>
                </Menu>
            </React.Fragment>
        );
    }
}
const mapStateToProps = state => ({
    currentUser: state.auth.user,
    isLoading: state.auth.isLoading,
})

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        logout
    },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Header));
