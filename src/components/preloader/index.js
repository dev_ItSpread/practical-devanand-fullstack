import React from 'react';
import { connect } from 'react-redux';
import './preloader.css';

const Preloader = ({ preloader, local }) => {
    return (
        <React.Fragment>
            {(preloader || local) &&
                <div className="preloader">
                    <div className="preloader-spin"></div>
                </div>
            }
        </React.Fragment>
    )
}

const mapStateToProps = (state) => ({
    preloader: state.layout.preloader
});

export default connect(mapStateToProps)(Preloader);