import React from 'react';
import { amber, green } from '@material-ui/core/colors';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import {
    SnackbarContent,
    IconButton,
    Snackbar
} from '@material-ui/core';
import {
    Error as ErrorIcon,
    Info as InfoIcon,
    CheckCircle as CheckCircleIcon,
    Warning as WarningIcon,
    Close as CloseIcon
} from '@material-ui/icons';

const useStyles = makeStyles((theme) => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));


const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

export default function Toast(props) {
    const classes = useStyles();
    const { onDismissClick, text, variant } = props;
    const Icon = variantIcon[variant];

    return (
        <Snackbar
            open={true}
            autoHideDuration={6000}
            onClose={onDismissClick}
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right'
            }}
        >
            <SnackbarContent
                className={clsx(classes[variant])}
                aria-describedby="client-snackbar"
                action={
                    <React.Fragment>
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            style={{ right: 0 }}
                            className={classes.close}
                            onClick={onDismissClick}
                        >
                            <CloseIcon className={clsx(classes.icon, "btn__closeSnack")} size="small" />
                        </IconButton>
                    </React.Fragment>
                }
                message={
                    <span className={clsx("client-snackbar", classes.message)}>
                        <Icon className={clsx(classes.icon, classes.iconVariant)} />
                        <span className="error_message">{text}</span>
                    </span>
                }
            />
        </Snackbar>
    )
};

