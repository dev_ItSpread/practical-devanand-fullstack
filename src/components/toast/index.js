import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { removeToast } from '../../redux/actions/toastActions';
import Toast from './components/toast';

class Toasts extends Component {
    render() {
        const { toasts, removeToast } = this.props;
        return (
            <React.Fragment>
                {toasts.map(toast => {
                    const { id } = toast;
                    return (
                        <Toast
                            {...toast}
                            key={id}
                            onDismissClick={() => removeToast(id)}
                        />
                    )
                })}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => ({
    toasts: state.toasts
});

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        removeToast
    },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(Toasts)
