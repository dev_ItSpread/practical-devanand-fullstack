import React from 'react'
import './loader.css';

export default () => (
    <div className="box">
        <div className="loader" />
        <p className="loading-text">Loading...</p>
    </div>
);

