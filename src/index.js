import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Switch, Route, BrowserRouter as Router, Redirect } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/styles';
import theme from './theme';
import FallbackSpinner from './components/fallback-loader';
import Preloader from './components/preloader';
import configureStore, { history } from './redux/store';
import { ConnectedRouter } from 'connected-react-router';
import ToastProvider from './components/toast';
import interseptResponse from './utils/axiosConfig';
import ProtectedRoute from './routes/ProtectedRoute';
import './styles/common.less';

const LoginPage = React.lazy(() => import('./pages/login'));
const RegisterPage = React.lazy(() => import('./pages/register'));
const DashboardPage = React.lazy(() => import('./pages/dashboard'));
const ProfilePage = React.lazy(() => import('./pages/profile'));
const AddStudent = React.lazy(() => import('./pages/addStudent'));
const store = configureStore();
interseptResponse(store);

const MainRouter = () => (
    <React.Fragment>
        <Switch>
            <Route exact path="/login" component={LoginPage}></Route>
            <Route exact path="/create-account" component={RegisterPage}></Route>
            <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
            <ProtectedRoute exact path="/dashboard" component={DashboardPage}></ProtectedRoute>
            <ProtectedRoute exact path="/profile" component={ProfilePage}></ProtectedRoute>
            <ProtectedRoute exact path="/addStudent" component={AddStudent}></ProtectedRoute>
        </Switch>
        <Preloader />
        <ToastProvider />
    </React.Fragment>
)


ReactDOM.render(
    <Provider store={store}>
        <ConnectedRouter history={history}>
            <ThemeProvider theme={theme}>
                <Router>
                    <Suspense fallback={<FallbackSpinner />}>
                        <MainRouter></MainRouter>
                    </Suspense>
                </Router>
            </ThemeProvider>
        </ConnectedRouter>
    </Provider>
    ,
    document.getElementById('root')
);