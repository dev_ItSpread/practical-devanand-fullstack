import React, { Component } from 'react';
import {
    Paper,
    Box,
    Avatar,
    Button,
    withStyles,
    FormControlLabel,
    Checkbox,
    Grid,
    Typography,
    Container,
    InputAdornment,
    IconButton,
    Link
} from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { signup } from '../../redux/actions/authActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './register.less';

const styles = theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: theme.spacing(2),
        margin: 'auto',
        minWidth: 500,
        [theme.breakpoints.down('sm')]: {
            minWidth: 'auto',
            maxWidth: 'auto'
        }
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        [theme.breakpoints.down('sm')]: {
            paddingLeft: theme.spacing(1),
            paddingRight: theme.spacing(1)
        }
    },
    textField: {
        marginTop: theme.spacing(1),
        marginBottom: 'auto'
    },
    submit: {
        margin: theme.spacing(2, 0)
    },
});


class SignUp extends Component {
    state = {
        showPassword: false,
        showConfirmPassword: false,
        formData: {
            firstName: "",
            lastName: "",
            email: "",
            password: "",
            confirmPassword: "",
            aggreeOnTermsCondition: false
        }
    }

     static getDerivedStateFromProps(nextProps, prevState) {
        if (!nextProps.isLoading && !!nextProps.currentUser && !nextProps.error) {
            nextProps.history.push('/');
        }

        return null;
    }

    componentDidMount() {
        const { formData } = this.state;
        ValidatorForm.addValidationRule('isPasswordMatch', (value) => {
            if (value !== formData.password) return false;
            return true;
        });
    }

    componentWillUnmount() {
        ValidatorForm.removeValidationRule('isPasswordMatch');
    }

    handleChange = ({ target } = event) => {
        const { formData } = this.state;
        if (target.type === "checkbox") {
            formData[target.name] = target.checked;
            this.setState({ formData });
        } else {
            formData[target.name] = target.value;
            this.setState({ formData });
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.signup(this.state.formData);
    }

    handleClickShowPassword = (val, ele) => this.setState({ [ele]: val });


    render() {
        const { classes } = this.props;
        const { formData, showPassword, showConfirmPassword } = this.state;

        return (
            <Box
                component="div"
                className="register__root"
                display="flex"
                justifyContent="center"
                alignItems="center"
                minHeight="100vh"
            >
                <Container maxWidth="xs">
                    <Paper className={classes.paper}>
                        <Avatar className={classes.avatar}><LockOutlinedIcon /></Avatar>
                        <Typography component="h1" variant="h5">Sign up</Typography>
                        <ValidatorForm
                            className={classes.form}
                            ref="form"
                            onSubmit={this.handleSubmit}
                        >
                            <Grid container spacing={1}>
                                <Grid item xs={12} sm={6}>
                                    <TextValidator
                                        fullWidth
                                        value={formData.firstName}
                                        onChange={this.handleChange}
                                        type="text"
                                        name="firstName"
                                        className={classes.textField}
                                        label="First Name"
                                        margin="normal"
                                        variant="outlined"
                                        validators={['required']}
                                        errorMessages={['First name should be filled!']}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={6}>
                                    <TextValidator
                                        fullWidth
                                        value={formData.lastName}
                                        type="text"
                                        onChange={this.handleChange}
                                        name="lastName"
                                        className={classes.textField}
                                        label="Last Name"
                                        margin="normal"
                                        variant="outlined"
                                        validators={['required']}
                                        errorMessages={['Last name should be filled!']}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextValidator
                                        fullWidth
                                        value={formData.email}
                                        onChange={this.handleChange}
                                        name="email"
                                        className={classes.textField}
                                        type="text"
                                        label="Email Address"
                                        margin="normal"
                                        variant="outlined"
                                        validators={['required', 'isEmail']}
                                        errorMessages={['Please enter email!', 'Email address is not valid!']}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextValidator
                                        fullWidth
                                        value={formData.password}
                                        onChange={this.handleChange}
                                        name="password"
                                        type={showPassword ? "text" : "password"}
                                        className={classes.textField}
                                        label='Password'
                                        margin="normal"
                                        variant="outlined"
                                        validators={['required']}
                                        errorMessages={['Please enter password!']}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        name="showPassword"
                                                        aria-label="Toggle password visibility"
                                                        onClick={() => this.handleClickShowPassword(!showPassword, 'showPassword')}
                                                    >
                                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextValidator
                                        fullWidth
                                        value={formData.confirmPassword}
                                        onChange={this.handleChange}
                                        name="confirmPassword"
                                        type={showConfirmPassword ? "text" : "password"}
                                        className={classes.textField}
                                        label='Confirm Password'
                                        margin="normal"
                                        variant="outlined"
                                        validators={['required', 'isPasswordMatch']}
                                        errorMessages={['Confirm password is required!', 'Password mismatch!']}
                                        InputProps={{
                                            endAdornment: (
                                                <InputAdornment position="end">
                                                    <IconButton
                                                        name="showConfirmPassword"
                                                        aria-label="Toggle password visibility"
                                                        onClick={() => this.handleClickShowPassword(!showConfirmPassword, 'showConfirmPassword')}
                                                    >
                                                        {showConfirmPassword ? <VisibilityOff /> : <Visibility />}
                                                    </IconButton>
                                                </InputAdornment>
                                            ),
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <FormControlLabel
                                        label="Agree on terms and conditions of expresscart"
                                        control={
                                            <Checkbox
                                                type="checkbox"
                                                onChange={this.handleChange}
                                                name="aggreeOnTermsCondition"
                                                value={formData.aggreeOnTermsCondition}
                                                color="primary"
                                            />
                                        }
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                            >
                                Sign Up
                            </Button>
                            <Grid container justify="flex-end">
                                <Grid item>
                                    <Link component={RouterLink} to="/login" variant="body2">Already have an account? Sign in</Link>
                                </Grid>
                            </Grid>
                        </ValidatorForm>
                    </Paper>
                </Container>
            </Box>
        );
    }
};

const mapStateToProps = state => ({
    currentUser: state.auth.user,
    isLoading: state.auth.isLoading,
    error: state.auth.error,
})


const mapDispatchToProps = dispatch => bindActionCreators(
    {
        signup
    },
    dispatch
);


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(SignUp));