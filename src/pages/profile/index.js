import React, { Component } from 'react';
import { Drawer, Paper, Avatar, Divider, Grid } from '@material-ui/core';
import {
    withStyles
} from '@material-ui/core';
import userAvatar from '../../assets/images/avatar/man.png';

const styles = theme => ({
    drawer: {
        width: 500,
        top: 65,
        height: 'calc(100% - 118px)',
        [theme.breakpoints.down('md')]: {
            width: "100%",
            height: 'calc(100% - 117px)',
        },
        [theme.breakpoints.down('sm')]: {
            height: 'calc(100% - 117px)',
        },
        [theme.breakpoints.down('xs')]: {
            top: 57,
            height: 'calc(100%)',
        }
    },
    paperRoot: {
        height: "100%",
        borderRadius: 0,
        padding: "70px 20px 40px 20px"
    },
    profileRoot: {
        background: "#ffffff",
        borderRadius: 3,
        boxShadow: "0 0 4px 0px rgba(0, 0, 0, 0.10)",
        padding: 15,
        position: "relative"
    },
    avatar: {
        width: theme.spacing(14),
        height: theme.spacing(14),
        position: "absolute",
        top: -46,
        borderRadius: 100,
        border: "10px solid #ffffff"
    },
    divider: {
        width: "100%",
    },
    updateProfileRoot: {
        marginTop: 22,
        background: "#ffffff",
        borderRadius: 3,
        boxShadow: "0 0 4px 0px rgba(0, 0, 0, 0.10)",
        padding: 15,
        position: "relative"
    }
})

class Profile extends Component {
    state = {
        openProfileDrawer: false
    }

    componentDidMount() {
        this.setState({ openProfileDrawer: true })
    }

    componentWillUnmount() {
        this.setState({ openProfileDrawer: false })
    }

    toggleDrawer = () => {
        this.setState({ openProfileDrawer: !this.state.openProfileDrawer })
    }

    render() {
        const { classes } = this.props;
        const { openProfileDrawer } = this.state;

        return (
            <Drawer
                classes={{ paper: classes.drawer }}
                anchor="right"
                variant="persistent"
                open={openProfileDrawer}
                onClose={this.toggleDrawer}
            >
                <Paper className={classes.paperRoot}>
                    <Grid
                        container
                        direction="row"
                        alignItems="center"
                        justify="center"
                        spacing={2}
                        className={classes.profileRoot}
                    >
                        <Grid item xs={12}>
                            <Avatar alt="Remy Sharp" src={userAvatar} className={classes.avatar}></Avatar>
                        </Grid>
                        <Grid item xs={12}>
                            {/* <Divider className={classes.divider} /> */}
                        </Grid>
                        <Grid item xs={12} style={{ marginTop: 10 }}>
                            Name
                        </Grid>
                        <Grid item xs={12}>
                            Email
                        </Grid>
                        <Grid item xs={12}>
                            Birth Date
                        </Grid>
                    </Grid>
                    <Grid
                        container
                        direction="row"
                        alignItems="center"
                        justify="center"
                        spacing={2}
                        className={classes.updateProfileRoot}
                    ></Grid>
                </Paper>
            </Drawer>
        );
    }
}

export default withStyles(styles)(Profile);
