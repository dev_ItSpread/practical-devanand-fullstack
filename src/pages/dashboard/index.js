import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import {
    withStyles
} from '@material-ui/core';

const styles = theme => ({
    root: {
        padding: theme.spacing(2)
    }
})

class Dashbaord extends Component {
    render() {
        const { classes } = this.props;
        return (
            <div className={classes.root}>
                <Grid container spacing={4}>
                    <Grid item lg={6} sm={6} xl={6} xs={6}>

                        Dashbaord
                    </Grid>
                    <Grid item lg={6} sm={6} xl={6} xs={6}>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

export default withStyles(styles)(Dashbaord);
