import React, { Component } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import {
    Paper,
    withStyles,
    Button,
    Box,
    IconButton,
    Typography,
    Link,
    Container,
    InputAdornment
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";
import { Visibility, VisibilityOff } from '@material-ui/icons';
import { signin } from '../../redux/actions/authActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import './login.less';

const styles = theme => ({
    paper: {
        padding: theme.spacing(2),
        margin: 'auto',
        maxWidth: 500,
        minWidth: 400,
        [theme.breakpoints.down('sm')]: {
            minWidth: 'auto',
            maxWidth: 'auto'
        }
    },
    form: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
        [theme.breakpoints.down('sm')]: {
            paddingLeft: theme.spacing(1),
            paddingRight: theme.spacing(1)
        }
    },
    title: {
        fontWeight: 600,
        lineHeight: "41px"
    },
    infoText: {
        marginTop: 5
    },
    textField: {
        marginTop: theme.spacing(2),
        marginBottom: 'auto'
    },
    textFieldPassWord: {
        marginBottom: theme.spacing(1)
    },
    btnSignIn: {
        margin: theme.spacing(2, 0)
    },
});

class Login extends Component {
    state = {
        showPassword: false,
        formData: {
            email: "",
            password: "",
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (!nextProps.isLoading && !!nextProps.currentUser && !nextProps.error) {
            nextProps.history.push('/');
        }

        return null;
    }

    handleChange = (event) => {
        const { formData } = this.state;
        formData[event.target.name] = event.target.value;
        this.setState({ formData });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.signin(this.state.formData)
    }

    handleClickShowPassword = () => {
        this.setState(prevState => {
            return {
                ...prevState,
                showPassword: prevState.showPassword ? false : true
            }
        })
    }

    render() {
        const { classes } = this.props;
        const { formData, showPassword } = this.state;
        const infoMessage = this.props.location.state && this.props.location.state.message;

        return (
            <Box
                component="div"
                className="login__root"
                display="flex"
                justifyContent="center"
                alignItems="center"
                minHeight="100vh"
            >
                <Container component="main" maxWidth="xs">
                    <Paper className={classes.paper}>
                        <ValidatorForm
                            className={classes.form}
                            ref="form"
                            onSubmit={this.handleSubmit}
                        >
                            <Typography className={classes.title} variant="h4">Sign in</Typography>
                            {infoMessage && <Alert className={classes.infoText} color="info" severity="info">{infoMessage}</Alert>}
                            <TextValidator
                                fullWidth
                                value={formData.email}
                                onChange={this.handleChange}
                                name="email"
                                className={classes.textField}
                                label="Email Address"
                                margin="normal"
                                variant="outlined"
                                validators={['required', 'isEmail']}
                                errorMessages={['Please enter email', 'Email address is not valid']}
                            />
                            <TextValidator
                                fullWidth
                                value={formData.password}
                                onChange={this.handleChange}
                                name="password"
                                type={showPassword ? "text" : "password"}
                                className={`${classes.textField} ${classes.textFieldPassWord}`}
                                id="outlined-name"
                                label='Password'
                                margin="normal"
                                variant="outlined"
                                validators={['required']}
                                errorMessages={['Please enter password']}
                                InputProps={{
                                    endAdornment: (
                                        <InputAdornment position="end">
                                            <IconButton
                                                aria-label="Toggle password visibility"
                                                onClick={this.handleClickShowPassword}
                                            >
                                                {showPassword ? <Visibility /> : <VisibilityOff />}
                                            </IconButton>
                                        </InputAdornment>
                                    ),
                                }}
                            />
                            <Box component="div" display="flex" flexDirection="row" justifyContent="space-between">
                                <Box>
                                    <Typography variant="body2">
                                        <Link component={RouterLink} variant="body2" to="/create-account">Register Yourself ! </Link>
                                    </Typography>
                                </Box>
                            </Box>

                            <Button
                                className={classes.btnSignIn}
                                color="primary"
                                fullWidth
                                size="large"
                                type="submit"
                                variant="contained"
                            >
                                Sign in now
                        </Button>
                        </ValidatorForm>
                    </Paper>
                </Container>
            </Box>
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.auth.user,
    isLoading: state.auth.isLoading,
    error: state.auth.error,
})

const mapDispatchToProps = dispatch => bindActionCreators(
    {
        signin
    },
    dispatch
);


export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(Login));