import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { autoLogin } from '../redux/actions/authActions';
import FallbackSpinner from '../components/fallback-loader';
import Layout from '../components/layout';
import { bindActionCreators } from 'redux';

class ProtectedRoute extends Component {
    componentDidMount() {
        const { currentUser, autoLogin } = this.props;
        if (!currentUser) {
            autoLogin();
        }
    }

    render() {
        const { component: Component, currentUser, autoLoggingIn, ...rest } = this.props;

        return (
            <Route
                {...rest}
                render={props => {
                    if (autoLoggingIn) {
                        return <FallbackSpinner />;
                    }

                    return !!currentUser ?
                        <Layout {...props}>
                            <Component {...props} />
                        </Layout>
                        :
                        <Redirect to="/login" />
                }}
            />
        );
    }
}

const mapStateToProps = state => ({
    currentUser: state.auth.user,
    autoLoggingIn: state.auth.autoLoggingIn
})

const mapDispatchToProps = dispatch => {
    return bindActionCreators(
        { autoLogin },
        dispatch
    )
}

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute);
