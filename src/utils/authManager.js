//Save Authentication
export const saveAuthentication = authentication => {
    const expiresAt = JSON.stringify(authentication.expires_in * 1000 + new Date().getTime());
    localStorage.setItem('access_token', authentication.access_token);
    localStorage.setItem('expires_at', expiresAt);
}

//Clear Authentication
export const clearAuthentication = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('expires_at');
}

export const isAuthenticated = () => {
    const accessToken = localStorage.getItem('access_token');
    const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    return !!accessToken && expiresAt > new Date().getTime();
}

export const getAccessToken = () => localStorage.getItem('access_token');
export const getBearerToken = () => {
    return 'Bearer ' + getAccessToken();
}