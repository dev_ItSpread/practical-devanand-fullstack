import axios from 'axios';
import { handleExpiredToken } from '../redux/actions/authActions';

axios.defaults.baseURL = 'http://localhost:5000';
const UNAUTHORIZED = 401;

const interseptResponse = store => {
    axios.interceptors.response.use(
        response => response,
        error => {
            if (error.response.status === UNAUTHORIZED) {
                store.dispatch(handleExpiredToken());
            }

            return Promise.reject(error);
        }
    )
}

export default interseptResponse;