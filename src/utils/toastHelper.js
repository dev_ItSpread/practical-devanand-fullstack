let id = 0;

export function createToast(options) {
    return {
        ...options,
        id: id++
    }
}