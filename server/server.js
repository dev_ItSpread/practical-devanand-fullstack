const express = require('express');
const mongoose = require('mongoose');
require('dotenv').config();
const config = require('./config/environment');
const app = express();

//Server create
const server = require('http').createServer(app);
require('./config/express')(app);
require('./routes')(app);


//Handle database connection
(async function serverListening() {
    try {
        //Database connection
        await mongoose.connect(config.mongo.uri, config.mongo.options)
        console.log('MongoDB connected')

        //Server listen
        server.listen(config.port, config.ip, () => {
            console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
        });
    } catch (error) {
        throw Error(error)
    }
})()

