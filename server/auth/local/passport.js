const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

exports.setup = function (User, config) {
    passport.use(new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        },
        (email, password, callback) => {
            User.findOne({ email }, (err, user) => {
                if (err) return callback(err);

                if (!user) {
                    return callback(null, false, { message: 'Email is not registred, please try again!' })
                }

                if (!user.authenticate(password)) {
                    return callback(null, false, { message: 'Password is not correct, please try again!' })
                }

                return callback(null, user);
            })
        })
    )
}
