const express = require('express');
const passport = require('passport');
const router = express.Router();
const auth = require('../auth.service');

//Local passport signin
router.post('/', (req, res, next) => {
    try {
        passport.authenticate('local', async (err, user, info) => {
            const error = err || info;
            if (error) {
                return res.status(401).json({
                    status: 401,
                    message: error.message || "Unauthorized!"
                })
            }

            if (!user) {
                return res.status(404).json({
                    status: 404,
                    message: 'User not found'
                })
            }

            const token = auth.signToken(user.token);
            return res.status(200).json({
                access_token: token,
                expires_in: auth.tokenExpireIn
            })
        })(req, res, next)
    } catch (error) {
        res.status(500).send(error.message || error)
    }
})

module.exports = router;