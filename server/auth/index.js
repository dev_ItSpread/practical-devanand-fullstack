'use strict';

const express = require('express');
const config = require('../config/environment')
const User = require('../api/user/user.model')

//Passport configuration
require('./local/passport').setup(User, config);

//Setup route
var router = express.Router();
router.use('/', require('./local'));

module.exports = router;
