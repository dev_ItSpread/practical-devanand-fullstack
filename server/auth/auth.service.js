const User = require('../api/user/user.model');
const config = require('../config/environment');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
const compose = require('composable-middleware');
const validateJwt = expressJwt({ secret: config.secrets.session, algorithms: ['HS256'] });

const tokenExpireIn = 2 * 60 * 60; //2 hours

const signToken = payload => {
    return jwt.sign(payload, config.secrets.session, { expiresIn: tokenExpireIn });
}

const isAuthenticated = () => {
    return (
        compose()
            .use(validateJwt)
            .use((req, res, next) => {
                if (!req.user) {
                    res.status(401).json({
                        message: 'Unauthorized!'
                    })
                }

                next();
            })
            .use((err, req, res, next) => {
                if (err && err.name === "UnauthorizedError") {
                    res.status(401).send({
                        message: err.message
                    })
                }
            })
            .use((req, res, next) => {
                try {
                    User.findById({ _id: req.user._id }, (err, user) => {
                        if (err) return next(err);
                        if (!user) {
                            return res.status(401).json({
                                message: 'Unauthorized',
                            });
                        }

                        req.user = user;
                        next();
                    })
                } catch (error) {
                    next(error);
                }
            })
    )
}

module.exports = {
    signToken,
    tokenExpireIn,
    isAuthenticated
}