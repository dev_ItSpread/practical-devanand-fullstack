var crypto = require('crypto');
var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    hashedPassword: String,
    phone: {
        type: String
    },
    salt: String,
    lastLogin: Date,
}, {
    timestamps: true
})

if (!UserSchema.options.toObject) UserSchema.options.toObject = {};
UserSchema.options.toObject.transform = function (doc, ret) {
    delete ret.hashedPassword;
    delete ret.salt;
    return ret;
};


/** Virtuals fields */
UserSchema.virtual('password').set(function (password) {
    this._password = password;
    this.salt = this.makeSalt();
    this.hashedPassword = this.encryptPassword(password);
}).get(function () {
    return this._password;
})

UserSchema.virtual('fullname').get(function () {
    return this.firstName + ' ' + this.lastName;
})

UserSchema.virtual('profile').get(function () {
    return {
        name: this.fullname,
        email: this.email
    }
})

UserSchema.virtual('token').get(function () {
    return {
        _id: this._id,
        ...this.profile
    }
})

UserSchema.methods = {
    makeSalt: function () {
        return crypto.randomBytes(16).toString('base64');
    },
    authenticate: function (password) {
        return this.encryptPassword(password) === this.hashedPassword;
    },
    encryptPassword: function (password) {
        if (!password || !this.salt) return '';
        var salt = Buffer.from(this.salt, 'base64');
        return crypto.pbkdf2Sync(password, salt, 10000, 512, 'sha512').toString('base64');
    }
}

module.exports = mongoose.model('User', UserSchema)