const User = require('./user.model');
const _ = require('lodash');
const auth = require('../../auth/auth.service');

/** user sign-up */
exports.create = async (req, res) => {
    try {
        let user = await User.findOne({ email: req.body.email });
        if (user) {
            user = _.merge(user, req.body);
        } else {
            user = new User(req.body);
        }

        //saving changes
        await user.save();

        //generate authentication token
        const token = auth.signToken(user.token); //virtual field

        res.status(200).json({
            access_token: token,
            expires_in: auth.tokenExpireIn,
        });
    } catch (error) {
        console.log('Sign up error - ', error);
        res.status(500).send(error.message || error);
    }
}

/* fetch profile */
exports.me = async (req, res) => {
    try {
        let user = req.user;
        user.lastLogin = new Date();
        await user.save();
        res.status(200).json(user);
    } catch (error) {
        console.log('Fetch profile Error - ', error);
        res.status(500).send(error.message || error);
    }
}