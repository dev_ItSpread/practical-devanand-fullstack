const express = require('express');
const router = express.Router();
const auth = require('../../auth/auth.service');
const controller = require('./student.controller');

// New User Registration
router.post('/register', controller.create);

//Fetch User Profile
router.get('/profile', auth.isAuthenticated(), controller.me);

module.exports = router;