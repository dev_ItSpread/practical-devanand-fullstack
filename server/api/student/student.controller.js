const Student = require('./student.model');
const _ = require('lodash');
const auth = require('../../auth/auth.service');

/** student sign-up */
exports.create = async (req, res) => {
    try {
        let student = await Student.findOne({ email: req.body.email });
        if (student) {
            student = _.merge(student, req.body);
        } else {
            student = new User(req.body);
        }

        //saving changes
        await student.save();


        res.status(200).json({
            student: student,
        });
    } catch (error) {
        console.log('Sign up error - ', error);
        res.status(500).send(error.message || error);
    }
}

/* fetch profile */
exports.me = async (req, res) => {
    try {
        let user = req.user;
        user.lastLogin = new Date();
        await user.save();
        res.status(200).json(user);
    } catch (error) {
        console.log('Fetch profile Error - ', error);
        res.status(500).send(error.message || error);
    }
}