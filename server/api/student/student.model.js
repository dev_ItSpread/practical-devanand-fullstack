var crypto = require('crypto');
var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentSchema = new Schema({
    firstName: String,
    lastName: String,
    email: {
        type: String,
        unique: true,
        lowercase: true
    },
    phone: {
        type: String
    },
}, {
    timestamps: true
})

if (!StudentSchema.options.toObject) StudentSchema.options.toObject = {};
StudentSchema.options.toObject.transform = function (doc, ret) {
    delete ret.salt;
    return ret;
};


/** Virtuals fields */


StudentSchema.virtual('fullname').get(function () {
    return this.firstName + ' ' + this.lastName;
})

StudentSchema.virtual('profile').get(function () {
    return {
        name: this.fullname,
        email: this.email
    }
})

StudentSchema.virtual('token').get(function () {
    return {
        _id: this._id,
        ...this.profile
    }
})

StudentSchema.methods = {
    makeSalt: function () {
        return crypto.randomBytes(16).toString('base64');
    }
}

module.exports = mongoose.model('Student', StudentSchema)