const mongoose = require('mongoose');
const fs = require('fs');
const path = require("path");
const config = require('../config/environment');
const Product = require('../api/product/product.model');
const ProductDetail = require('../api/product/productdetail.model');

(async function migrate() {
    try {
        await mongoose.connect(config.mongo.uri, config.mongo.options);
        const productsData = await fs.readFileSync(path.join(__dirname, 'products.json'), 'utf8');
        const { products } = JSON.parse(productsData);
        console.log('Initialize migration...')

        if (products.length) {
            products.map(async product => {
                let productObj = {
                    name: product.title,
                    description: product.description,
                    price: product.price
                }

                let savedProduct = await new Product(productObj).save()

                let productDetail = {
                    product_id: savedProduct._id,
                    size: product.availableSizes
                }

                await new ProductDetail(productDetail).save()
            })
            console.log('Migration over!!')

        }
    } catch (error) {
        console.log('Product Migration Error -', error);
    }
})()