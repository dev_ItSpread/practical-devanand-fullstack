const path = require('path');
const _ = require('lodash');

const all = {
    env: process.env.NODE_ENV,

    // Root path of server
    root: path.normalize(__dirname + '/../../..'),

    // Server port
    port: process.env.PORT || 9000,

    // Secret for session, you will want to change this and make it an environment variable
    secrets: {
        session: process.env.SECRET
    },

    // MongoDB connection options
    mongo: {
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
    },
}


// Export the config object based on the NODE_ENV
module.exports = _.merge(all, require('./' + process.env.NODE_ENV + '.js') || {});