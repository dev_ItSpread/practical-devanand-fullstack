const express = require('express');
const morgan = require('morgan');
const compression = require('compression');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const cookieParser = require('cookie-parser');
const path = require('path');
const config = require('./environment');
const helmet = require('helmet');

module.exports = app => {
    const env = app.get('env');
    //Middlewars
    app.use(compression());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(cookieParser());

    if ('production' === env) {
        app.use(express.static(path.join(config.root, 'build')));
        app.set('appPath', path.join(config.root, 'build'));
        app.use(morgan('dev'));
        app.use(helmet());
        app.disable('x-powered-by');

        // CORS middleware (for production only)
        app.use((req, res, next) => {
            // CORS header
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');

            // Set custom headers for CORS
            res.header(
                'Access-Control-Allow-Headers',
                'Content-type,Accept,X-Access-Token,X-Key, Authorization'
            );
            if (req.method == 'OPTIONS') {
                res.status(200).end();
            } else {
                next();
            }
        });
    }


    if ('development' === env || 'test' === env) {
        app.use(express.static(path.join(config.root, 'build')));
        app.set('appPath', path.join(config.root, 'build'));
        app.use(morgan('dev'));
        app.use((req, res, next) => {
            // CORS header
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');

            // Set custom headers for CORS
            res.header(
                'Access-Control-Allow-Headers',
                'Content-type,Accept,X-Access-Token,X-Key, Authorization'
            );
            if (req.method == 'OPTIONS') {
                res.status(200).end();
            } else {
                next();
            }
        });
    }
}