const userRoutes = require('./api/user');
const studentRoutes = require('./api/student');

module.exports = app => {
    //Routes
    app.use('/api/users', userRoutes);
    app.use('/api/authenticate', require('./auth'));
    app.use('/api/addStudent',studentRoutes);

    // All other routes should redirect to the index.html
    app.route('/*').get((req, res) => {
        res.sendFile(app.get('appPath') + '/index.html');
    });
}